from django.urls import path
from .views import libViews, dataJson, tambahLikes, leaderboard

urlpatterns = [
    path('', libViews),
    path('datajson/', dataJson),
    path('addLikes/', tambahLikes),
    path('leaderboard/', leaderboard),
]
