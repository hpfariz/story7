from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def libViews(request):
    return render(request, 'library.html')

def dataJson(request):
    try:
        q = request.GET['q']
    except:
        q = "quitting"
    
    hasil = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()

    return JsonResponse(hasil)

def tambahLikes(request):
    data = {}
    if(request.method=="GET"):
        bookID = request.GET["bookID"]
        #bookIDSession = request.session["bookID"]
        try:
            request.session[bookID] += 1
            request.session["ids2"][bookID] += 1
        except KeyError:
            request.session[bookID] = 1
            try:
                request.session["ids2"][bookID] = 1
            except KeyError:
                request.session["ids2"] = {bookID:1}

        data = {
            'jumlahLikes': request.session[bookID],
            'listID' : request.session['ids2']
        }

    return JsonResponse(data)

def leaderboard(request):
    rank = {}
    if(request.method=="GET"):
        leaderboard_temp = request.session["ids2"]
        # print(leaderboard_temp)
        for i in range(5):
            max_temp = max(leaderboard_temp, key = leaderboard_temp.get)
            rank[max_temp] = leaderboard_temp[max_temp]
            del leaderboard_temp[max_temp]
            if leaderboard_temp == {}:
                return JsonResponse(rank)

    return JsonResponse(rank)
