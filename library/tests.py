from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import libViews
from selenium import webdriver
import unittest
import time
from selenium.webdriver.chrome.options import Options
from django.http import HttpRequest
from selenium.webdriver.common.keys import Keys


# Create your tests here.
class UnitTest(TestCase):

    def test_url_is_exist(self):
        response = self.client.get('/library/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_fires_correct_views_method(self):
        reseponse = resolve('/library/')
        self.assertEqual(reseponse.func, libViews)
    
    def test_views_render_correct_html(self):
        response = self.client.get('/library/')
        self.assertTemplateUsed(response, 'library.html')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()

    def test_search(self):
        self.selenium.get(self.live_server_url+'/library')

        self.assertIn("LIBRARY", self.selenium.page_source)

        # driver = webdriver.Chrome()
        # driver.get
        search = self.selenium.find_element_by_id("search")
        search.send_keys('as')
        search.send_keys(Keys.ENTER)
        time.sleep(5)
        print(self.selenium.find_element_by_id("result").text)
        # print(self.selenium.page_source)
        # getOutput = self.selenium.execute_script(search.send_keys('as'))
        print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        # print(getOutput)
        # self.assertIn("Tentara Bayaran AS di Irak", getOutput)
        
        ####### test gagal ######