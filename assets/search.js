$(document).ready(function() {
    $("#search").on("keypress", function(e) {
        if(e.which == 13){
            var q = e.currentTarget.value.toLowerCase()
            $.ajax({
                url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
                success: function(data) {
                    $('#result').html('')
                    var dataBuku = '';
                    for (var i = 0; i < data.items.length; i++) {
                        dataBuku += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                            "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" + 
                            "<td class='align-middle'><p id='" + data.items[i].id + "'>Click the Button!</p><button onclick='tambah(\"" + (data.items[i].id) + "\")' id='" + data.items[i].id + "'>Like</button></td>"+ 
                            " </tr>";
                    }
                    $('#result').append(dataBuku);
                },
                error: function(error) {
                    alert("Books not found");
                }
            })
        }
    });
});

function tambah(id){
    $.ajax(
        {
            url: "addLikes",
            datatype: 'json',
            data: {'bookID' : id},
            success: function(jumlahLikesTambah){
                console.log(jumlahLikesTambah);
                $("#"+id).html(jumlahLikesTambah.jumlahLikes);
                $("#isiLeaderboard").html("")
            }
        }
    )
}

function leaderboard(){
    $.ajax(
        {
            url: "leaderboard",
            success: function(listID){
                console.log(listID);
                var hasil = "";
                var i = 1
                var list_temp = listID;
                for(var key in listID){
                    console.log(key);
                    $.ajax({
                        url: "https://www.googleapis.com/books/v1/volumes?q="+key,
                        success: function(data){
                            console.log(key);
                            var temp = ""
                            temp = "<tr> <th scope='row' class='align-middle text-center'>" + (i) + "</th>" +
                            "<td><img class='img-fluid' style='width:22vh' src='" + data.items[0].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                            "<td class='align-middle'>" + data.items[0].volumeInfo.title + "</td>" +
                            "<td class='align-middle'>" + data.items[0].volumeInfo.authors + "</td>" +
                            "<td class='align-middle'>" + list_temp[key] + "</td>" + 
                            " </tr>";
                            i += 1;
                            $('#isiLeaderboard').append(temp);
                        }
                    })
                    console.log(key);
                }
            }
        }
    )
}