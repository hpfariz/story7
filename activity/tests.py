from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from .apps import ActivityConfig
from django.http import HttpRequest
from .views import activity
from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(ActivityConfig.name, 'activity')
        self.assertEqual(apps.get_app_config('activity').name, 'activity')


class UnitTesting(TestCase):
    def test_activity_url_is_exist(self):
        response = Client(). get ('')
        self.assertEqual(response.status_code, 200)

    def test_page_template(self):
        response = self.client.get("/activity/")
        self.assertTemplateUsed(response,'activity.html')
    
    def test_greetings(self):
        request = HttpRequest()
        response = activity(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Halo! Saya Fariz Habibie",html_response)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()

    def test_accordion(self):
        self.selenium.get(self.live_server_url+'/activity')

        #Cek isi accordion setelah diklik
        self.selenium.find_element_by_id('firstValue').click()
        self.assertIn("- Kuliah Semester 2 dengan 23 SKS", self.selenium.page_source)
        self.selenium.find_element_by_id('secValue').click()
        self.assertIn("- Staf Humas dan Publikasi Open House Fasilkom 2019", self.selenium.page_source)
        self.selenium.find_element_by_id('thirdValue').click()
        self.assertIn("Tidak ada", self.selenium.page_source)
        self.selenium.find_element_by_id('fourthValue').click()
        self.assertIn("- Champion of Mini Case Competition di COMPFEST X", self.selenium.page_source)

    def test_button(self):
        self.selenium.get(self.live_server_url+'/activity')

        #Cek perpindahan accordion setelah diklik
        self.selenium.find_element_by_id("atasSecond").click()
        text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(text.find("Pengalaman Kepanitiaan") > text.find("Aktivitas Saat Ini"))
        
        self.selenium.find_element_by_id("atasThird").click()
        text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(text.find("Pengalaman Organisasi") > text.find("Aktivitas Saat Ini"))

    def test_theme(self):
        self.selenium.get(self.live_server_url+'/activity')
        # self.selenium.find_element_by_class_name('titik1').click()
        bg = self.selenium.find_element_by_name('body').value_of_css_property('background-color')
        self.assertIn('rgba(255, 255, 255, 1)', bg)
        self.selenium.find_element_by_id('tema1').click()
        bg = self.selenium.find_element_by_name('body').value_of_css_property('background-color')
        self.assertIn('rgba(255, 255, 255, 1)', bg)
        self.assertIn('<div class="titik" id="tema1" name="tema1"', self.selenium.page_source)
        self.assertIn('<div class="titik" id="tema2" name="tema2"', self.selenium.page_source)
