from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from .forms import formAkun

# Create your views here.
def userLogin(request):
    if request.method == "POST":
        form = formAkun(request.POST)
        if (form.is_valid()):
            userName = form.cleaned_data["user_name"]
            passWord = form.cleaned_data["pass_word"]
            user = authenticate(request, username = userName, password = passWord)
            if user is not None:
                login(request, user)
        return redirect("/story9/")
    else:
        form = formAkun()
        response = {
            "form" : form
        }
        return render(request, "login.html", response)


def userRegist(request):
    if request.method == "POST":
        form = formAkun(request.POST)
        if (form.is_valid()):
            userName = form.cleaned_data["user_name"]
            passWord = form.cleaned_data["pass_word"]
            try:
                user1 = User.objects.get(username = userName)
                return redirect("/story9/register/")
            except User.DoesNotExist:
                user = User.objects.create_user(userName, None, passWord)
                return redirect("/story9/")
        else:
            return redirect("/story9/register/")
    else:
        form = formAkun()
        response = {
            "form" : form
        }
        return render(request, "register.html", response)


def userLogout(request):
    logout(request)
    return redirect('/story9/')