 
from django.urls import path
from .views import userLogin, userRegist, userLogout

urlpatterns = [
    path('', userLogin),
    path('register/', userRegist),
    path('logout/', userLogout)
]