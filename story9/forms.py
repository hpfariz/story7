from django import forms

class formAkun(forms.Form):
    user_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Username',
        'type' : 'text',
        'required' : True
    }))
    pass_word = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Password',
        'type' : 'password',
        'required' : True
    }))
