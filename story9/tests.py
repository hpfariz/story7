from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from .views import userLogin, userRegist, userLogout

# Create your tests here.

class UnitTest(TestCase):
    def test_url_is_exist(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/story9/register/')
        self.assertEqual(response.status_code, 200)


    def test_using_correct_template(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'login.html')
        response = self.client.get('/story9/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_template_elements(self):
        request = HttpRequest()
        response = userLogin(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Login Page",html_response)

    def test_trying_login(self):
        response = self.client.post('/story9/', data={'user_name' : 'coba', 'pass_word' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Hello, coba')
    
    def test_trying_register_with_existed_account(self):
        response = self.client.post('/story9/register/', data={'user_name' : 'coba', 'pass_word' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Login')

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
    
    def test_main(self):
        self.selenium.get(self.live_server_url + '/story9')
        a = self.selenium.get(self.live_server_url + '/story9')
        print("aaaaaaaaaaaaaa = ",a)

        #Test First Login (didn't register)
        self.selenium.find_element_by_name('user_name').send_keys('coba')
        self.selenium.find_element_by_name('pass_word').send_keys('coba')
        self.selenium.find_element_by_name('button-login').click()
        time.sleep(5)
        self.assertIn('Login Page', self.selenium.page_source)

        #Test Register
        self.selenium.find_element_by_name('register').click()
        time.sleep(5)
        self.selenium.find_element_by_name('user_name').send_keys('coba')
        self.selenium.find_element_by_name('pass_word').send_keys('coba')
        self.selenium.find_element_by_name('button-register').click()
        self.assertIn('Login Page', self.selenium.page_source)

        #Test Login (registered)
        self.selenium.find_element_by_name('user_name').send_keys('coba')
        self.selenium.find_element_by_name('pass_word').send_keys('coba')
        self.selenium.find_element_by_name('button-login').click()
        time.sleep(5)
        self.assertIn('Hello, coba', self.selenium.page_source)

        #Test Logout
        self.selenium.find_element_by_name('button-logout').click()
        time.sleep(5)
        self.assertIn('Login Page', self.selenium.page_source)
