from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .apps import PesanConfig
from .models import Message
from .forms import MessageForm, ConfirmationForm
from .views import homepage, confirmation
import time

# Create your tests here.

class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(PesanConfig.name, 'pesan')
        self.assertEqual(apps.get_app_config('pesan').name, 'pesan')

# UNIT TEST
class testPesan(TestCase):
    def test_pesan_url_is_exist(self):
        response = Client(). get ('')
        self.assertEqual(response.status_code, 200)

    def test_confirmation_url_exist(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_object(self):
        # Test buat objek baru
        input_test = Message(nama="Fariz", pesan="test bro")
        input_test.save()
        self.assertTrue(isinstance(input_test, Message))
        self.assertEqual("a shoutout by Fariz", str(input_test))

        # Hitung jumlah objek setelah dibuat
        jumlah = Message.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_form_validation_for_blank_items(self):
        form = MessageForm(data={'nama': '', 'pesan': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['nama'], ["This field is required."])
        self.assertEqual(form.errors['pesan'], ["This field is required."])

    def test_form_validation_accepted(self):
        form = MessageForm(
            data={'nama': 'BukanFariz', 'pesan': 'semoga testnya berhasil'})
        self.assertTrue(form.is_valid())

    def test_redirect_after_a_POST_request(self):
        response = self.client.post(
            '', data={'nama': 'Fariz', 'pesan': 'test POST'})
        Client().get('/confirmation/')
       
    def test_submitted_changed_after_saving_form(self):
        response = Client().get('/?submitted=True')
        self.assertEqual(homepage.submitted, True)

    def test_confirmation_buttons_working(self):
        self.client.post('/confirmation/')

        # Check if form is valid
        form = ConfirmationForm(
            data={'action': 'yes'})
        self.assertTrue(form.is_valid())

        # Test if false
        self.client.post('/confirmation/', data={'action': 'no'})
        # Test if true
        response = self.client.post(
            '', data={'nama': 'Fariz', 'pesan': 'test POST'})
        self.client.post('/confirmation/', data={'action': 'yes'})

class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTesting, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTesting, self).tearDown()

    def test_walkthrough_false(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Shout it out!', self.browser.title)
        self.browser.find_element_by_class_name('main')

        # User bakal nemuin header ajakan ngisi form
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('give yourself\na SHOUTOUT', header_text)

        # Nyoba input di form
        nama = self.browser.find_element_by_name('nama')
        nama.send_keys('cobafariz')
        pesan = self.browser.find_element_by_name('pesan')
        pesan.send_keys('helowold')
        submit = self.browser.find_element_by_class_name('submit')
        time.sleep(5)
        submit.click()

        # User bakal keredirect ke web konfirmasi
        self.assertIn('Confirmation', self.browser.title)
        time.sleep(5)

        # Pencet tombol konfirmasi
        rejected = self.browser.find_element_by_class_name('rejected')
        rejected.click()

        # Ada container buat nampilin message-message yang sebelumnya pernah dikirim orang
        self.assertIn('Shout it out!', self.browser.title)
        self.assertNotIn('tezzz', self.browser.page_source)

    def test_walkthrough_true(self):
        # Waktu user buka pagenya, ada tulisan Spread Your Message di title
        self.browser.get(self.live_server_url)
        self.assertIn('Shout it out!', self.browser.title)
        self.browser.find_element_by_class_name('main')

        # User bakal nemuin header ajakan ngisi form
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('give yourself\na SHOUTOUT', header_text)

        # Nyoba input di form
        nama = self.browser.find_element_by_name('nama')
        nama.send_keys('cobafariz')
        pesan = self.browser.find_element_by_name('pesan')
        pesan.send_keys('helowold')
        submit = self.browser.find_element_by_class_name('submit')
        time.sleep(5)
        submit.click()

        # User bakal keredirect ke web konfirmasi
        self.assertIn('Confirmation', self.browser.title)
        time.sleep(5)

        # Pencet tombol konfirmasi
        confirmed = self.browser.find_element_by_class_name('confirmed')
        confirmed.click()

        # Ada container buat nampilin message-message yang sebelumnya pernah dikirim orang
        self.assertIn('Shout it out!', self.browser.title)
        preview_box = self.browser.find_element_by_class_name('prev_box')
        preview_name = self.browser.find_element_by_class_name('prev_name')
        preview_message = self.browser.find_element_by_class_name(
            'prev_message')
