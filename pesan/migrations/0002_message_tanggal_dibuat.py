# Generated by Django 3.0.5 on 2020-04-12 07:47

from django.db import migrations, models
import django


class Migration(migrations.Migration):

    dependencies = [
        ('pesan', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='tanggal_dibuat',
            field=models.DateTimeField(auto_now_add=True, default="2020-04-12 14:55:00.000000"),
            preserve_default=False,
        ),
    ]
