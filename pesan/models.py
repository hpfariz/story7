from django.db import models
import django

# Create your models here.
class Message(models.Model):
    nama = models.CharField('input your name here', max_length = 50)
    pesan = models.TextField('input your message here')
    tanggal_dibuat = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "a shoutout by " + self.nama