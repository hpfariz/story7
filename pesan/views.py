from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm, ConfirmationForm
import random

# Create your views here.
nama = ""
pesan = ""
def homepage(request):
    global nama, pesan
    homepage.submitted = False
    input_list = Message.objects.all().order_by('tanggal_dibuat').reverse()
    homepage.form = MessageForm()
    if request.method == 'POST':
        homepage.form = MessageForm(request.POST)
        if homepage.form.is_valid():
            nama = homepage.form.cleaned_data['nama']
            pesan = homepage.form.cleaned_data['pesan']
            return redirect('/confirmation/')
    else:
        homepage.form = MessageForm()
        if 'submitted' in request.GET:
            homepage.submitted = True
    return render(request, 'homepage.html', {'form': homepage.form, 'submitted': homepage.submitted, 'list': input_list})

def confirmation(request):
    if request.method == 'POST':
        response = ConfirmationForm(request.POST)
        if response.is_valid():
            if response.cleaned_data['action'] == 'yes':
                homepage.form.save()
                return redirect('/?submitted=True')
            elif response.cleaned_data['action'] == 'no':
                return redirect('/')

    return render(request, 'confirmation.html', {'nama': nama, 'pesan': pesan})