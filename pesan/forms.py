from django import forms
from django.forms import ModelForm, Textarea
from .models import Message


class MessageForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Message
        fields = '__all__'
        widgets = {
            'nama': Textarea(attrs={'cols': 40, 'rows': 1}),
            'pesan': Textarea(attrs={'cols': 40, 'rows': 5}),
            'tanggal_dibuat': forms.HiddenInput(),
        }

class ConfirmationForm(forms.Form):
    action = forms.CharField()