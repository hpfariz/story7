from django.urls import path
from . import views

app_name = 'pesan'

urlpatterns = [
    path('', views.homepage, name='homepage'),
     path('confirmation/', views.confirmation, name='confirmation'),
]